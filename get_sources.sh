#!/bin/sh

ID=${1}
FEATUREVER=17

if [ "x${ID}" = "x" ] ; then
    echo "$0 <ID>";
    exit 1;
fi

if [ "x${TMPDIR}" = "x" ] ; then
    TMPDIR=/tmp
fi

downloaddir=${TMPDIR}/download.$$
mkdir ${downloaddir}
pushd ${downloaddir}
"Downloading build ${ID} in ${downloaddir}";
brew download-build ${ID}

versionregexp="[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*-[0-9]*"
basename=$(ls|grep java-${FEATUREVER}-openjdk-portable-${versionregexp}.el7openjdkportable.x86_64.rpm)
version=$(echo ${basename}|sed -r "s|^.*-(${versionregexp})\.el7.*$|\1|")

echo "Downloaded version ${version}"

# Remove stripped release builds for portable
rm -vf java-${FEATUREVER}-openjdk-portable-${FEATUREVER}*
rm -vf java-${FEATUREVER}-openjdk-portable-devel-${FEATUREVER}*

for file in *.rpm; do
    rpm2archive ${file};
done

mkdir unpacked
for file in *.tgz; do
    tar -C unpacked -xzf ${file};
done

mkdir ${HOME}/${version}
mv unpacked/usr/lib/jvm/* ${HOME}/${version}

pushd ${HOME}/${version}
for file in *.sha256sum; do
    if ! sha256sum --check ${file} ; then
	echo "${file} failed checksum.";
	exit 2;
    fi
done
popd

rm -rf unpacked
rm -vf *.tgz
rm -vf *.rpm

popd

